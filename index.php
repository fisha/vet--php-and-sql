<?php
	session_start();
	
	if((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']== true))
	{
		header('Location: panel.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">

</head>

<body>
	
	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="#">Strona główna</a></li>
				<li><a href="#">Nasza kadra</a></li>
				<li><a href="#">Usługi</a></li>
				<li><a href="#">Kontakt</a></li>
				<li><a href="#">Logowanie</a></li>
			</ul>
		</nav>	
	</header>
	
	<main>
		<div class="formularz">
			
			<form action="zaloguj.php" method="post">
			
				<input type="text" name="login" placeholder="Login" onfocus="this.placeholder=''" onblur="this.placeholder='Login'"/>
				<input type="password" name="haslo" placeholder="Hasło" onfocus="this.placeholder=''" onblur="this.placeholder='Hasło'"/>
				<input type="submit" value="Zaloguj się"/>
			
			</form>
			
			<a href="rejestracja.php">Nie masz konta? Zarejestruj się!</a>
		</div>
		
	</main>
	
<?php

	if(isset($_SESSION['blad'])) echo $_SESSION['blad'];
	
?>


</body>


</html>