<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="css/fontello.css" type="text/css" />
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="panel.php">Moje konto</a></li>
				<li><a href="pokazmoich.php">Moi pacjenci</a></li>
				<li><a href="pokazwszystkich.php">Wszyscy pacjenci</a></li>
				<li><a href="fdodaj.php">Dodaj pacjenta</a></li>
				<li><a href="logout.php">Wyloguj się</a></li>
			</ul>
		</nav>	
	</header>


<?php
	require_once "connect.php";
	
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
	
	if($polaczenie->connect_errno!=0)
	{
		echo "Error: ".$polaczenie->connect_errno . " Opis: ".$polaczenie->connect_error;
	}
	else
	{

		$id_zw = $_SESSION['id_zw'];
		$gatunek=$_POST['gatunek'];
		$rasa=$_POST['rasa'];
		$masc=$_POST['masc'];
		$imie_zw=$_POST['imie_zw'];
		$waga=$_POST['waga'];
		$data_ur=$_POST['data_ur'];
		
		$imie_wlasc=$_POST['imie_wlasc'];
		$nazwisko_wlasc=$_POST['nazwisko_wlasc'];
		$adres=$_POST['adres'];
		$miasto=$_POST['miasto'];
		$telefon_wlasc=$_POST['telefon_wlasc'];
		$email_wlasc=$_POST['email_wlasc'];
		$id_wlasc = $_POST['id_wlasc'];
			
			
		$sql = "UPDATE zwierzeta SET gatunek='$gatunek', rasa='$rasa', masc='$masc', imie_zw='$imie_zw', waga=$waga, data_ur='$data_ur' WHERE id_zw='$id_zw'";
		
		if ($rezultat = @$polaczenie->query($sql))
		{
			
		} else {
				
			echo '<span style="color:red">Błąd edycji klienta!</span>';
			header('Location: panel.php');
				
		}			
			
			
			

		$sql = "UPDATE klienci SET imie_wlasc='$imie_wlasc', nazwisko_wlasc='$nazwisko_wlasc', adres='$adres',
				miasto='$miasto', telefon_wlasc='$telefon_wlasc', email_wlasc='$email_wlasc' WHERE id_wlasc='$id_wlasc'";
		
		if ($rezultat = @$polaczenie->query($sql))
		{
			echo "Edycja zakończona sukcesem!";
			
		} else {
				
			echo '<span style="color:red">Błąd edycji klienta!</span>';
			header('Location: panel.php');
				
		}
		
		echo "<table>
				<td>".$id_zw."</td>
				<td>".$gatunek."</td>
				<td>".$rasa."</td>
				<td>".$masc."</td>
				<td>".$imie_zw."</td>
				<td>".$waga."</td>
				<td>".$data_ur."</td>
				<td>".$imie_wlasc." ".$nazwisko_wlasc."</td>
				<td>".$email_wlasc."</td>
				<td>".$adres."</td>
				<td>".$miasto."</td>
				<td>".$telefon_wlasc."</td>
				</tr>
				</table>";
		
		
		$polaczenie->close();
	}
?>


</body>


</html>