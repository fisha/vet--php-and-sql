<?php
	session_start();
	
	if(isset($_POST['email']))
	{
		// Udana walidacja? Zakładamy, że tak
		$wszystko_OK=true;
		
		//Zmienne służące do sprawdzenia poprawności
		$nick = $_POST['nick'];

		
//sprawdzenie poprawności nicka
		if(strlen($nick)<3 || strlen($nick)>20)
		{
			$wszystko_OK = false;
			$_SESSION['e_nick'] = "Nick musi posiadać od 3 do 20 znaków";
		}
		
		if(ctype_alnum($nick)==false)
		{
			$wszystko_OK = false;
			$_SESSION['e_nick'] = "Nick może skłać się tylko z liter i cyfr(bez polskich znaków)";
		}
		
//sprawdzenie poprawności adresu email
		$email = $_POST['email'];
		$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);
		
		if(filter_var($emailB, FILTER_VALIDATE_EMAIL)== false || $email!=$emailB)
		{
			$wszystko_OK = false;
			$_SESSION['e_email'] = "Podaj poprawny adres email!";
		}
		
//sprawdzenie poprawności hasla
		$haslo1 = $_POST['haslo1'];
		$haslo2 = $_POST['haslo2'];
		
		if(strlen($haslo1)<8 || strlen($haslo1)>20)
		{
			$wszystko_OK = false;
			$_SESSION['e_haslo'] = "Hasło musi posiadać od 8 do 20 znaków";
		}
		
		if($haslo1!=$haslo2)
		{
			$wszystko_OK = false;
			$_SESSION['e_haslo'] = "Podane hasła nie są identyczne";
		}		
		
		$haslo_hash = password_hash($haslo1, PASSWORD_DEFAULT);	
		
//sprawdzenie checkboxa
		if(!isset($_POST['regulamin']))
		{
			$wszystko_OK = false;
			$_SESSION['e_regulamin'] = "Potwierdź akceptację regulaminu";
		}

//Imię, Nazwisko, telefon i funkcja
		$imie = $_POST['imie'];
		$nazwisko = $_POST['nazwisko'];
		$telefon = $_POST['telefon'];

		
// sprawdzenie captchy
		$s_key = "6Lcu8bcUAAAAANmfMyA6nKXuAT8FtqD3AFCJTB5B";
		
		$sprawdz = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$s_key.'&response='.$_POST['g-recaptcha-response']);
		
		$odpowiedz = json_decode($sprawdz);
		
		if($odpowiedz->success== false)
		{
			$wszystko_OK = false;
			$_SESSION['e_bot'] = "Potwierdź, że nie jesteś botem";
		}		

// zapamiętaj wprowadzone dane w formularzu
		$_SESSION['fr_nick'] = $nick;
		$_SESSION['fr_email'] = $email;
		$_SESSION['fr_haslo1'] = $haslo1;
		$_SESSION['fr_haslo2'] = $haslo2;
		$_SESSION['fr_imie'] = $imie;
		$_SESSION['fr_nazwisko'] = $nazwisko;
		$_SESSION['fr_telefon'] = $telefon;
		if(isset($_POST['regulamin'])) $_SESSION['fr_regulamin'] = true;
		

//sprawdzenie, czy nie istnieje już inne konto przypisane do danego maila
		require_once "connect.php";
		
		mysqli_report(MYSQLI_REPORT_STRICT); //wyłącza wyświetlanie błędów na stronie(tak, żeby user nie widział 'root'
		
		try
		{
			$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
			if($polaczenie->connect_errno!=0)
			{
				throw new Exception(mysqli_connect_errno());
			}	
			else
			{
				//tworzymy tabelę
				
				
				// czy email już istnieje?
				$rezultat = $polaczenie->query("SELECT id_wet FROM weterynarze WHERE email_wet='$email'");
				
				if(!$rezultat) throw new Exception($polaczenie->error);
				
				$ile_takich_maili = $rezultat->num_rows;
				if($ile_takich_maili>0)
				{
					$wszystko_OK = false;
					$_SESSION['e_email'] = "Istnieje już konto przypisane do tego adresu email";		
				}
				
				// czy nick jest już zarezerwowany?
				$rezultat = $polaczenie->query("SELECT id_wet FROM weterynarze WHERE login='$nick'");
				
				if(!$rezultat) throw new Exception($polaczenie->error);
				
				$ile_takich_userow = $rezultat->num_rows;
				if($ile_takich_userow>0)
				{
					$wszystko_OK = false;
					$_SESSION['e_nick'] = "Istnieje już gracz o takim nicku. Wybierz inny.";		
				}
				
				// OSTATECZNE SPRAWDZENIE, CZY WSZYSTKO OK	
				if($wszystko_OK==true)
				{
					// super, wszystkie walidacje zakończone sukcesem, próbujemy wrzucić nowego usera do bazy
					
					if($polaczenie->query("INSERT INTO weterynarze VALUES (NULL, '$nick', '$haslo_hash',
					'$imie','$nazwisko','$telefon','$email')"))
					{
						$_SESSION['udanarejestracja'] = true;
						header('Location: witamy.php');
					}
					else
					{
						throw new Exception($polaczenie->error);
					}
					
				}
				
				$polaczenie->close();
				
			}
			
		}
		catch(Exception $e)
		{
			echo '<span style="color: red;">Błąd serwera! Przepraszamy za niedogosności i prosimy o rejestrację w innym terminie!</span>';
			echo '<br />Informacja deweloperska: '.$e;
		}

		
	}
	
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
	<style>
		.error
		{
			color: red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	
	</style>
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="#">Strona główna</a></li>
				<li><a href="#">Nasza kadra</a></li>
				<li><a href="#">Usługi</a></li>
				<li><a href="#">Kontakt</a></li>
				<li><a href="#">Logowanie</a></li>
			</ul>
		</nav>	
	</header>

	<form method="post">
	
		Twój login: <br /><input type="text" name="nick" value="<?php
			if(isset($_SESSION['fr_nick']))
			{
				echo $_SESSION['fr_nick'];
				unset ($_SESSION['fr_nick']);
			}
		?>"/><br />
		
		<?php
			if(isset($_SESSION['e_nick']))
			{
				echo '<div class="error">'.$_SESSION['e_nick'].'</div>';
				unset($_SESSION['e_nick']);
			}
		?>	
		
		Imię: <br /><input type="text" name="imie" required value="<?php
			if(isset($_SESSION['fr_imie']))
			{
				echo $_SESSION['fr_imie'];
				unset ($_SESSION['fr_imie']);
			}
		?>"/><br />
		
		Nazwisko: <br /><input type="text" name="nazwisko" required value="<?php
			if(isset($_SESSION['fr_nazwisko']))
			{
				echo $_SESSION['fr_nazwisko'];
				unset ($_SESSION['fr_nazwisko']);
			}
		?>"/><br />
		
		Telefon: <br /><input type="tel" name="telefon" required value="<?php
			if(isset($_SESSION['fr_telefon']))
			{
				echo $_SESSION['fr_telefon'];
				unset ($_SESSION['fr_telefon']);
			}
		?>"/><br />
		
		
		E-mail: <br /><input type="text" name="email" value="<?php
			if(isset($_SESSION['fr_email']))
			{
				echo $_SESSION['fr_email'];
				unset ($_SESSION['fr_email']);
			}
		?>"/><br />
		
		<?php
			if(isset($_SESSION['e_email']))
			{
				echo '<div class="error">'.$_SESSION['e_email'].'</div>';
				unset($_SESSION['e_email']);
			}
		?>
		
		Twoje hasło: <br /><input type="password" name="haslo1" value="<?php
			if(isset($_SESSION['fr_haslo1']))
			{
				echo $_SESSION['fr_haslo1'];
				unset ($_SESSION['fr_haslo1']);
			}
		?>"/><br />
		
		<?php
			if(isset($_SESSION['e_haslo']))
			{
				echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
				unset($_SESSION['e_haslo']);
			}
		?>		
		
		Powtórz hasło: <br /><input type="password" name="haslo2" value="<?php
			if(isset($_SESSION['fr_haslo2']))
			{
				echo $_SESSION['fr_haslo2'];
				unset ($_SESSION['fr_haslo2']);
			}
		?>"/><br /><br />
		
		
		<label><input type="checkbox" name="regulamin" <?php
		if(isset($_SESSION['fr_regulamin']))
		{
			echo "checked";
			unset($_SESSION['fr_regulamin']);
		}
		?>/> Akceptuję regulamin </label><br /><br />
		
		<?php
			if(isset($_SESSION['e_regulamin']))
			{
				echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
				unset($_SESSION['e_regulamin']);
			}
		?>		
		
		<div class="g-recaptcha" data-sitekey="6Lcu8bcUAAAAALLiltQNWwFpV0heGY_02UrNnXWx"></div>

		<?php
			if(isset($_SESSION['e_bot']))
			{
				echo '<div class="error">'.$_SESSION['e_bot'].'</div>';
				unset($_SESSION['e_bot']);
			}
		?>
		
		<br />
		


		
		<input type="submit" value="Zarejestruj się" />
		
	
	</form>
	


</body>


</html>