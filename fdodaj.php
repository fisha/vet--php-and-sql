<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="panel.php">Moje konto</a></li>
				<li><a href="pokazmoich.php">Moi pacjenci</a></li>
				<li><a href="pokazwszystkich.php">Wszyscy pacjenci</a></li>
				<li><a href="fdodaj.php">Dodaj pacjenta</a></li>
				<li><a href="logout.php">Wyloguj się</a></li>
			</ul>
		</nav>	
	</header>


<div class="formularz">

	<form action="dodaj.php" method="post">
		
		Nowy pacjent 
			
		<label><input type="text" name="gatunek" placeholder="Gatunek" onfocus="this.placeholder=''" onblur="this.placeholder='Gatunek'"></label>
		<label><input type="text" name="rasa" placeholder="Rasa" onfocus="this.placeholder=''" onblur="this.placeholder='Rasa'"></label>
		<label><input type="text" name="masc" placeholder="Maść" onfocus="this.placeholder=''" onblur="this.placeholder='Maść'"></label>
		<label><input type="text" name="imie_zw" placeholder="Imię zwierzęcia" onfocus="this.placeholder=''" onblur="this.placeholder='Imię zwierzęcia'"></label>
		<label><input type="number" step="0.1" name="waga" placeholder="Waga" onfocus="this.placeholder=''" onblur="this.placeholder='Waga'"></label>
		<label><input type="text" name="data_ur" placeholder="Data urodzenia" onfocus="this.type='date'" onblur="this.type='text'" onblur="this.placeholder='Data urodzenia'"></label>
		<label><input type="text" name="imie_wlasc" placeholder="Imię właściciela" onfocus="this.placeholder=''" onblur="this.placeholder='Imię właściciela'"></label>
		<label><input type="text" name="nazwisko_wlasc" placeholder="Nazwisko właściciela" onfocus="this.placeholder=''" onblur="this.placeholder='Nazwisko właściciela'"></label>
		<label><input type="text" name="adres" placeholder="Adres" onfocus="this.placeholder=''" onblur="this.placeholder='Adres'"></label>
		<label><input type="text" name="miasto" placeholder="Miasto" onfocus="this.placeholder=''" onblur="this.placeholder='Miasto'"></label>
		<label><input type="tel" name="telefon_wlasc" placeholder="Telefon" onfocus="this.placeholder=''" onblur="this.placeholder='Telefon'"></label>
		<label><input type="email" name="email_wlasc" placeholder="E-mail" onfocus="this.placeholder=''" onblur="this.placeholder='E-mail'"></label>
		<input type="submit" value="Zapisz">
				
	</form>
	
</div>




</body>


</html>