<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="css/fontello.css" type="text/css" />
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="panel.php">Moje konto</a></li>
				<li><a href="pokazmoich.php">Moi pacjenci</a></li>
				<li><a href="pokazwszystkich.php">Wszyscy pacjenci</a></li>
				<li><a href="fdodaj.php">Dodaj pacjenta</a></li>
				<li><a href="logout.php">Wyloguj się</a></li>
			</ul>
		</nav>	
	</header>


<?php
	require_once "connect.php";
	
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
	
	if($polaczenie->connect_errno!=0)
	{
		echo "Error: ".$polaczenie->connect_errno . " Opis: ".$polaczenie->connect_error;
	}
	else
	{

		$_SESSION['id_zw'] = $_POST['id'];
		$id_zw = $_SESSION['id_zw'];

//zwierzeta.id_zw, zwierzeta.gatunek, zwierzeta.rasa, zwierzeta.masc, zwierzeta.imie_zw, zwierzeta.waga, zwierzeta.data_ur, klienci.imie, klienci.nazwisko, klienci.email, klienci.adres, klienci.miasto, klienci.telefon FROM zwierzeta,klienci
			
			
		$sql = "SELECT * FROM zwierzeta,klienci WHERE klienci.id_wlasc=zwierzeta.id_wlasc AND zwierzeta.id_zw='$id_zw'";
		
		if ($rezultat = @$polaczenie->query($sql))
		{
			$ile_zwierzat = $rezultat->num_rows;
			if ($ile_zwierzat>0)
			{
				
				$wiersz = $rezultat->fetch_assoc();
				
				$id_zw = $wiersz['id_zw'];
				$gatunek = $wiersz['gatunek'];
				$rasa = $wiersz['rasa'];
				$masc = $wiersz['masc'];
				$imie_zw = $wiersz['imie_zw'];
				$waga = $wiersz['waga'];
				$data_ur = new DateTime($wiersz['data_ur']);	
				$imie_wlasc = $wiersz['imie_wlasc'];	
				$nazwisko_wlasc = $wiersz['nazwisko_wlasc'];	
				$email_wlasc = $wiersz['email_wlasc'];	
				$adres = $wiersz['adres'];	
				$miasto = $wiersz['miasto'];	
				$telefon_wlasc = $wiersz['telefon_wlasc'];
				$id_wlasc = $wiersz['id_wlasc'];
				
				
				echo 
				"<table class='pacjenci'>
					<thead>
						<tr>
							<th>Numer</th>
							<th>Gatunek</th>
							<th>Rasa</th>
							<th>Kolor</th>
							<th>Imię</th>
							<th>Waga</th>
							<th>Data ur</th>
							<th>Właściciel</th>
							<th>E-mail</th>
							<th>Ulica</th>
							<th>Miasto</th>
							<th>Telefon</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>".$id_zw."</td>
							<td>".$gatunek."</td>
							<td>".$rasa."</td>
							<td>".$masc."</td>
							<td>".$imie_zw."</td>
							<td>".$waga."</td>
							<td>".$data_ur->format('d-m-Y')."</td>
							<td>".$imie_wlasc." ".$nazwisko_wlasc."</td>
							<td>".$email_wlasc."</td>
							<td>".$adres."</td>
							<td>".$miasto."</td>
							<td>".$telefon_wlasc."</td>
						</tr>
					</tbody>
				</table>";
				
				
			} else {
				
				$_SESSION['blad'] = '<span style="color:red">Błąd odczytu zwierząt z bazy</span>';
				header('Location: panel.php');
				
			}
		}
		
		$polaczenie->close();
	}
?>

	<div class="formularz">
		<form action="edytuj.php" method="post">
		
			Edytuj 
				
			<label><input type="text" name="gatunek" placeholder="Gatunek" onfocus="this.placeholder=''" onblur="this.placeholder='Gatunek'"></label>
			<label><input type="text" name="rasa" placeholder="Rasa" onfocus="this.placeholder=''" onblur="this.placeholder='Rasa'"></label>
			<label><input type="text" name="masc" placeholder="Maść" onfocus="this.placeholder=''" onblur="this.placeholder='Maść'"></label>
			<label><input type="text" name="imie_zw" placeholder="Imię zwierzęcia" onfocus="this.placeholder=''" onblur="this.placeholder='Imię zwierzęcia'"></label>				<label><input type="number" step="0.1" name="waga" placeholder="Waga" onfocus="this.placeholder=''" onblur="this.placeholder='Waga'"></label>
			<label><input type="text" name="data_ur" placeholder="Data urodzenia" onfocus="this.type='date'" onblur="this.type='text'" onblur="this.placeholder='Data urodzenia'"></label>
			<label><input type="text" name="imie_wlasc" placeholder="Imię właściciela" onfocus="this.placeholder=''" onblur="this.placeholder='Imię właściciela'"></label>
			<label><input type="text" name="nazwisko_wlasc" placeholder="Nazwisko właściciela" onfocus="this.placeholder=''" onblur="this.placeholder='Nazwisko właściciela'"></label>
			<label><input type="text" name="adres" placeholder="Adres" onfocus="this.placeholder=''" onblur="this.placeholder='Adres'"></label>
			<label><input type="text" name="miasto" placeholder="Miasto" onfocus="this.placeholder=''" onblur="this.placeholder='Miasto'"></label>
			<label><input type="tel" name="telefon_wlasc" placeholder="Telefon" onfocus="this.placeholder=''" onblur="this.placeholder='Telefon'"></label>
			<label><input type="email" name="email_wlasc" placeholder="E-mail" onfocus="this.placeholder=''" onblur="this.placeholder='E-mail'"></label>
			<input type="hidden" name="id_wlasc" value="<?php echo $id_wlasc; ?>" />
			<input type="submit" value="Zapisz">
			
		</form>

	</div>



</body>


</html>