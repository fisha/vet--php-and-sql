<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="panel.php">Moje konto</a></li>
				<li><a href="pokazmoich.php">Moi pacjenci</a></li>
				<li><a href="pokazwszystkich.php">Wszyscy pacjenci</a></li>
				<li><a href="fdodaj.php">Dodaj pacjenta</a></li>
				<li><a href="logout.php">Wyloguj się</a></li>
			</ul>
		</nav>	
	</header>


<?php
	require_once "connect.php";
	
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
	
	
	if($polaczenie->connect_errno!=0)
	{
		echo "Error: ".$polaczenie->connect_errno . " Opis: ".$polaczenie->connect_error;
	}
	else
	{
		
		$gatunek=$_POST['gatunek'];
		$rasa=$_POST['rasa'];
		$masc=$_POST['masc'];
		$imie_zw=$_POST['imie_zw'];
		$waga=$_POST['waga'];
		$data_ur=$_POST['data_ur'];
		$imie_wlasc=$_POST['imie_wlasc'];
		$nazwisko_wlasc=$_POST['nazwisko_wlasc'];
		$adres=$_POST['adres'];
		$miasto=$_POST['miasto'];
		$telefon_wlasc=$_POST['telefon_wlasc'];
		$email_wlasc=$_POST['email_wlasc'];
				

		$sql= "INSERT INTO klienci VALUES(NULL,'$imie_wlasc','$nazwisko_wlasc','$adres','$miasto','$telefon_wlasc','$email_wlasc')";
		
		if ($rezultat = $polaczenie->query($sql))
		{
					
		} else {
				
				echo '<span style="color:red">Błąd dodawania klienta</span>';
				header('Location: panel.php');
		}
				
			
		
		$sql= "SELECT * FROM klienci where email_wlasc='$email_wlasc'";
		
		if ($rezultat = @$polaczenie->query($sql))
		{
			$ile_klientow = $rezultat->num_rows;
			if ($ile_klientow>0)
			{
				$wiersz = $rezultat->fetch_assoc();
				$id_wlasc = $wiersz['id_wlasc'];
				
			} else {
				
				$_SESSION['blad'] = '<span style="color:red">Błąd odczytu klientów</span>';
				header('Location: panel.php');
				
			}
		}
		
		
		$id_wet = $_SESSION['id_wet'];
	
		$sql= "INSERT INTO zwierzeta VALUES (NULL,'$id_wlasc','$id_wet','$gatunek','$rasa','$masc','$imie_zw',$waga,'$data_ur')";
		
		if ($rezultat = @$polaczenie->query($sql))
		{
			echo "Dodawanie zakończono sukcesem!";
				
		} else {
				
				$_SESSION['blad'] = '<span style="color:red">Błąd odczytu zwierząt z bazy</span>';
				header('Location: panel.php');
				
		}
		
		
		
		
		$polaczenie->close();
	}
?>



</body>


</html>