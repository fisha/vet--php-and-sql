<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="css/fontello.css" type="text/css" />
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="panel.php">Moje konto</a></li>
				<li><a href="pokazmoich.php">Moi pacjenci</a></li>
				<li><a href="pokazwszystkich.php">Wszyscy pacjenci</a></li>
				<li><a href="fdodaj.php">Dodaj pacjenta</a></li>
				<li><a href="logout.php">Wyloguj się</a></li>
			</ul>
		</nav>	
	</header>

<?php
	require_once "connect.php";
	
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
	
	if($polaczenie->connect_errno!=0)
	{
		echo "Error: ".$polaczenie->connect_errno . " Opis: ".$polaczenie->connect_error;
	}
	else
	{
		$wet_id=$_SESSION['id_wet'];

//zwierzeta.id_zw, zwierzeta.gatunek, zwierzeta.rasa, zwierzeta.masc, zwierzeta.imie_zw, zwierzeta.waga, zwierzeta.data_ur, klienci.imie, klienci.nazwisko, klienci.email, klienci.adres, klienci.miasto, klienci.telefon FROM zwierzeta,klienci
			
		$sql = "SELECT * FROM zwierzeta,klienci WHERE klienci.id_wlasc=zwierzeta.id_wlasc AND zwierzeta.id_wet_gl='$wet_id'";
		
		if ($rezultat = @$polaczenie->query($sql))
		{
			$ile_zwierzat = $rezultat->num_rows;
			if ($ile_zwierzat>0)
			{
				
				echo "<table class='pacjenci'>
						<thead>
							<tr>
								<th>Numer</th>
								<th>Gatunek</th>
								<th>Rasa</th>
								<th>Kolor</th>
								<th>Imię</th>
								<th>Waga</th>
								<th>Data ur</th>
								<th>Właściciel</th>
								<th>E-mail</th>
								<th>Ulica</th>
								<th>Miasto</th>
								<th>Telefon</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>";
				
				while(($wiersz = $rezultat->fetch_assoc()) !== NULL)
				{
					echo "<tr>";
					$id_zw = $wiersz['id_zw'];
					$gatunek = $wiersz['gatunek'];
					$rasa = $wiersz['rasa'];
					$masc = $wiersz['masc'];
					$imie_zw = $wiersz['imie_zw'];
					$waga = $wiersz['waga'];
					$data_ur = new DateTime($wiersz['data_ur']);	
					$imie_wlasc = $wiersz['imie_wlasc'];	
					$nazwisko_wlasc = $wiersz['nazwisko_wlasc'];	
					$email_wlasc = $wiersz['email_wlasc'];	
					$adres = $wiersz['adres'];	
					$miasto = $wiersz['miasto'];	
					$telefon_wlasc = $wiersz['telefon_wlasc'];

	
					
					echo 
					"<td>".$id_zw."</td>
					<td>".$gatunek."</td>
					<td>".$rasa."</td>
					<td>".$masc."</td>
					<td>".$imie_zw."</td>
					<td>".$waga."</td>
					<td>".$data_ur->format('d-m-Y')."</td>
					<td>".$imie_wlasc." ".$nazwisko_wlasc."</td>
					<td>".$email_wlasc."</td>
					<td>".$adres."</td>
					<td>".$miasto."</td>
					<td>".$telefon_wlasc.'</td>
					<td>
						<form action="fedytuj.php" method="post">
							<input type="hidden" name="id" value="'.$id_zw.'" />
							<button><i class="icon-pencil"></i></button>
						</form>
					</td>
					<td>
						<form action="usun.php" method="post">
							<input type="hidden" name="id" value="'.$id_zw.'" />
							<button><i class="icon-trash-empty"></i></button>
						</form>					
					</td>
					</tr>';
					
				}	
				
				echo "</tbody></table>";
				
				
			} else {
				
				$_SESSION['blad'] = '<span style="color:red">Błąd odczytu zwierząt z bazy</span>';
				header('Location: panel.php');
				
			}
		}
		
		$polaczenie->close();
	}
?>



</body>


</html>