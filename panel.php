<?php
	session_start();
	
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Klinika Weterynaryjna</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>

<body>

	<header>
	<h1 class="logo">Gdańska klinika weterynaryjna</h1>
		<nav id="topnav">
			<ul class="menu">
				<li><a href="panel.php">Moje konto</a></li>
				<li><a href="pokazmoich.php">Moi pacjenci</a></li>
				<li><a href="pokazwszystkich.php">Wszyscy pacjenci</a></li>
				<li><a href="fdodaj.php">Dodaj pacjenta</a></li>
				<li><a href="logout.php">Wyloguj się</a></li>
			</ul>
		</nav>	
	</header>

<?php

	echo "<p>Witaj ".$_SESSION['imie_wet'].'!';

	echo "<p><b>ID weterynarza: </b>".$_SESSION['id_wet']."<br />";
	echo "<p><b>Imię: </b>".$_SESSION['imie_wet']."<br />";
	echo "<p><b>Nazwisko: </b>".$_SESSION['nazwisko_wet']."<br />";
	echo "<p><b>E-mail: </b>".$_SESSION['email_wet']."<br />";
	echo "<p><b>Telefon: </b>".$_SESSION['telefon_wet']."<br />";

?>


</body>


</html>